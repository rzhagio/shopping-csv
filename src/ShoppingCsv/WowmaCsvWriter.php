<?php

namespace Rrrz\ShoppingCsv;

class WowmaCsvWriter {
    function writeToFile($data, $filepath) {
        $fp = fopen($filepath, 'w');

        $head = [
            'ctrlCol',
            'lotNumber',
            'itemName',
            'itemManagementId',
            'itemManagementName',
            'itemCode',
            'itemPrice',
            'taxSegment',
            'postageSegment',
            'postage',
            'deliveryId',
            'publicStartDate',
            'limitedOrderSegment',
            'limitedOrderCount',
            'description',
            'descriptionForSP',
            'descriptionForPC',
            'detailTitle',
            'detailDescription',
            'specTitle',
            'spec1',
            'spec2',
            'spec3',
            'spec4',
            'spec5',
            'searchKeyword1',
            'searchKeyword2',
            'searchKeyword3',
            'imageName1',
            'imageName2',
            'imageName3',
            'imageName4',
            'imageName5',
            'imageName6',
            'imageName7',
            'imageName8',
            'imageName9',
            'imageName10',
            'imageUrl1',
            'imageUrl2',
            'imageUrl3',
            'imageUrl4',
            'imageUrl5',
            'imageUrl6',
            'imageUrl7',
            'imageUrl8',
            'imageUrl9',
            'imageUrl10',
            'categoryId',
            'tagId',
            'jan',
            'isbn',
            'itemModel',
            'limitedPasswd',
            'limitedPasswdPageTitle',
            'limitedPasswdPageMessage',
            'saleStatus',
            'itemOption1',
            'itemOption2',
            'itemOption3',
            'itemOption4',
            'itemOption5',
            'pointRate',
            'stockSegment',
            'displayBackorderMessage',
            'stockCount',
            'displayStockSegment',
            'displayStockThreshold',
            'choicesStockHorizontalItemName',
            'choicesStockVerticalItemName',
            'choicesStockUpperDescription',
            'choicesStockLowerDescription',
            'displayChoicesStockSegment',
            'displayChoicesStockThreshold'
        ];
        fputcsv($fp, $head);
        foreach ($data as $row) {
            $wowma_row = [];
            foreach ($head as $key) {
                $wowma_row[] = isset($row[$key]) ? mb_convert_encoding($row[$key], 'SJIS-WIN', 'UTF-8') : '';
            }
            fputcsv($fp, $wowma_row);
        }

        fclose($fp);
    }
}
